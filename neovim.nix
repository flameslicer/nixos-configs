{ config, pkgs, ... }:

{
  programs.neovim = {
    enable = true;
    extraConfig  = ''
      set number
      set hidden

      let g:LanguageClient_serverCommands = {
      \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
      \ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
      \ 'javascript.jsx': ['tcp://127.0.0.1:2089'],
      \ 'python': ['/usr/local/bin/pyls'],
      \ 'ruby': ['~/.rbenv/shims/solargraph', 'stdio'],
      \ 'cpp': ['cquery', '--log-file=/tmp/cq.log'],
      \ 'c': ['cquery', '--log-file=/tmp/cq.log'],
      \ }

     let g:LanguageClient_loadSettings = 1 " Use an absolute configuration path if you want system-wide settings
     let g:LanguageClient_settingsPath = '~/.config/nvim/settings.json'
     set completefunc=LanguageClient#complete
     set formatexpr=LanguageClient_textDocument_rangeFormatting()

     nnoremap <silent> gh :call LanguageClient#textDocument_hover()<CR>
     nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
     nnoremap <silent> gr :call LanguageClient#textDocument_references()<CR>
     nnoremap <silent> gs :call LanguageClient#textDocument_documentSymbol()<CR>
     nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
    '';
    plugins = with pkgs.vimPlugins; [
      LanguageClient-neovim 
      deoplete-nvim
      deoplete-clang
    ];
  };
}
