{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  imports = [
    ./neovim.nix
  ];   
  home.packages = with pkgs; [
    libreoffice 
    discord
    multimc
    plank
    papirus-icon-theme
    polybarFull
    gnome3.file-roller
    transmission-gtk
    baobab
    xfce.xfce4-pulseaudio-plugin 
    openshot-qt
    simplescreenrecorder
    libsForQt511.vlc
    communi
    polkit_gnome
    libimobiledevice
    cava
    mpd
    mpc_cli
    neofetch
    vimpc
    jetbrains.idea-community
    cquery
    lolcat
];
}
